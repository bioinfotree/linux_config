;; inibit-startup-message
(setq inhibit-startup-message t)

;; FROM http://www.emacswiki.org/emacs/AutomaticFileHeaders#toc1
;; personal configuration for Automatic File Headers
;;
;; add additional path in wich search .el emacs configuration files 
(add-to-list 'load-path (expand-file-name "/home/michele/Research/Projects/Multi-Project_Script/emacs/"))
(require 'header2)
(add-hook 'emacs-lisp-mode-hook 'auto-make-header)
;;
;; FROM http://www.emacswiki.org/emacs/AutomaticFileHeaders#toc1


;; FROM http://code.google.com/p/yasnippet/
;; YASnippet template system for Emacs. See documentation
;;
(require 'yasnippet-bundle)
;;
;; FROM http://code.google.com/p/yasnippet/



;; FROM: http://www.linuxhelp.net/guides/emacs/#customize
;;
;; Set up the keyboard so the delete key on both the regular keyboard
;; and the keypad delete the character under the cursor and to the right
;; under X, instead of the default, backspace behavior.
(global-set-key [delete] 'delete-char)
(global-set-key [kp-delete] 'delete-char)

;; turn on font-lock mode
(global-font-lock-mode t)

;; enable visual feedback on selections
(setq-default transient-mark-mode t)

;; always end a file with a newline
(setq require-final-newline t)

;; stop at the end of the file, not just add lines
(setq next-line-add-newlines nil)

;;to display time
(display-time)

;;to set the cursor color
(set-cursor-color "red")

;;to set the font
;;(set-frame-font "-*-fixedsysttf-*-*-*-*-15-*-*-*-*-*-*-*")

;;to set foreground color to white
(set-foreground-color "white")

;;to set background color to black
(set-background-color "black")

;;to manage the geometric size of initial window.
(setq initial-frame-alist '((width . 87) (height . 42)))

;;set the keybinding so that you can use f4 for goto line
(global-set-key [f4] 'goto-line)
;;set the keybinding so that f3 will start the shell
(global-set-key [f3] 'shell)
;;set the keybinding so that f5 will start query replace
(global-set-key [f5] 'query-replace)
(global-set-key [f6] 'switch-to-buffer)
(global-set-key [f7] 'hippie-expand)
(global-set-key [f8] 'ispell)

;;(setq load-path (cons "/media/disk-1/Research/Projects/Transcriptomic_analysis/Src/" load-path))

;;change the default folder in emacs
(setq default-directory "/home/michele/Research/Projects/Transcriptomic_analysis/Src/" )

(when window-system
;; enable wheelmouse support by default
(mwheel-install)
;; use extended compound-text coding for X clipboard
(set-selection-coding-system 'compound-text-with-extensions))
(autoload 'mpg123 "mpg123" "A Front-end to mpg123" t)



(custom-set-variables
;; custom-set-variables was added by Custom -- don't edit or cut/paste it!
;; Your init file should contain only one such instance.
'(auto-save-default nil)
;;'(backup-directory-alist (quote (("." . "/home/hemant/backup"))))
;;'(default-frame-alist (quote ((tool-bar-lines . 1) (menu-bar-lines . 1) (width . 87) (height . 42)))))
)

;;(custom-set-faces
;; custom-set-faces was added by Custom -- don't edit or cut/paste it!
;; Your init file should contain only one such instance.
;;'(font-lock-comment-face ((((class color) (background dark)) (:foreground "chocolate1" :slant italic :family "-*-lucida-medium-r-*-*-14-*-*-*-*-*-*-*"))))
;;'(fringe ((((class color) (background dark)) (:background "grey10" :width ultra-condensed))))
;;'(scroll-bar ((t (:background "Dark slate gray")
;;))))
;;
;; FROM: http://www.linuxhelp.net/guides/emacs/#customize



;;redefines the keys to copy, paste, cut:
;;copy = Ctrl-Ctrl-z
;;(global-set-key "\C-z" 'clipboard-kill-region)
;;(global-set-key "\C-c" 'clipboard-kill-ring-save)
;;(global-set-key "\C-v" 'clipboard-yank)


;;other costumization
(transient-mark-mode t)
;;highlighting parentheses
(show-paren-mode 'true)
(line-number-mode t)
(column-number-mode t)


;;Reload .Emacs file when loaded into the current buffer
;;Alt-x eval-buffer

(put 'upcase-region 'disabled nil)

(put 'downcase-region 'disabled nil)
